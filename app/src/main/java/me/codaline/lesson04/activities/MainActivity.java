package me.codaline.lesson04.activities;

import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.Arrays;
import java.util.List;

import me.codaline.lesson04.R;
import me.codaline.lesson04.adapters.CustomAdapter;
import me.codaline.lesson04.entities.User;

public class MainActivity extends AppCompatActivity {

    private DrawerLayout mDrawerLayout;
    private View mViewDrawer;
    private ActionBarDrawerToggle mActionBarDrawerToggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initDrawer();

        initListView();
    }

    private void initDrawer() {
        mViewDrawer = findViewById(R.id.view_drawer);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mActionBarDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                //action on drawer close
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                //action on drawer open
            }
        };
        mDrawerLayout.setDrawerListener(mActionBarDrawerToggle);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        mActionBarDrawerToggle.syncState();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (mDrawerLayout.isDrawerOpen(mViewDrawer)) {
                    mDrawerLayout.closeDrawer(mViewDrawer);
                } else {
                    mDrawerLayout.openDrawer(mViewDrawer);
                }
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return false;
    }

    private void initListView() {
        ListView listView = (ListView) findViewById(R.id.list);
        List<User> userList = Arrays.asList(
                new User("John", "Connor", 16, "USA"),
                new User("Hermione", "Granger", 18, "UK, London"),
                new User("Luke", "Skywalker", 22, "planet Tatooine")
        );
        CustomAdapter adapter = new CustomAdapter(this, userList);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //do something
            }
        });
    }
}
