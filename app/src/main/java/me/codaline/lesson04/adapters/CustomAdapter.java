package me.codaline.lesson04.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import me.codaline.lesson04.R;
import me.codaline.lesson04.entities.User;

/**
 * Created by rage on 21.02.16.
 */
public class CustomAdapter extends ArrayAdapter<User> {

    public CustomAdapter(Context context, List<User> objects) {
        super(context, R.layout.item_user, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder;
        if (convertView == null) {
            convertView = View.inflate(getContext(), R.layout.item_user, null);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        User user = getItem(position);
        holder.txtName.setText(String.format("%s %s", user.getFirstName(), user.getLastName()));
        holder.txtAddress.setText(user.getAddress());
        holder.txtAge.setText(String.format("%d years", user.getAge()));

        return convertView;
    }

    private class Holder {
        TextView txtName;
        TextView txtAddress;
        TextView txtAge;

        public Holder(View rootView) {
            this.txtName = (TextView) rootView.findViewById(R.id.txt_name);
            this.txtAddress = (TextView) rootView.findViewById(R.id.txt_address);
            this.txtAge = (TextView) rootView.findViewById(R.id.txt_age);
        }
    }
}
